function createNewUser() {
    let firstName = prompt('what is your name?');
    let lastName = prompt('what is your lastname?');
    let birthday = prompt('your date of birthday?');

    let birthdayParts = birthday.split('.');
    let newUser = {
    firstName: firstName,
    lastName: lastName,
    birthday: birthday,

    getAge() {
        const today = new Date();
        const birthdate = new Date(birthdayParts[2],birthdayParts[1],birthdayParts[0]);
        let age = today.getFullYear() - birthdate.getFullYear();

        if (
        today.getMonth() < birthdate.getMonth() ||
        (today.getMonth() === birthdate.getMonth() && today.getDate() < birthdate.getDate())
        ) {
        age--;
        }

        return age;
    },

    getPassword() {
        let  birthYear = this.birthday.split('.')[2];
        let password = firstName[0].toUpperCase() + lastName.toLowerCase() + birthYear;

        return password;
    }
    };

    return newUser;
}

let user = createNewUser();
console.log(user);
console.log(user.getAge());
console.log(user.getPassword());